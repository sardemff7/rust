# Copyright 2021 Quentin ADAM <quentin.adam@waxzce.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=ellie ] cargo [ rust_minimum_version=1.67.0 ] \
        bash-completion zsh-completion

export_exlib_phases src_install pkg_postinst

SUMMARY="Magical shell history"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    fish-completion [[ description = [ Install completion files for the fish shell ] ]]
"
DEPENDENCIES=""

# Requires connection to "postgres://atuin:pass@localhost:5432/atuin"
# test/sync.rs
RESTRICT="test"

BASH_COMPLETIONS=( src/shell/${PN}.bash )
ZSH_COMPLETIONS=( src/shell/${PN}.zsh )

atuin_src_install() {
    cargo_src_install

    bash-completion_src_install
    zsh-completion_src_install
    if option fish-completion; then
        insinto /usr/share/fish
        newins src/shell/${PN}.fish ${PN}.fish
    fi
}

atuin_pkg_postinst() {
    einfo "atuin needs to be init in your shell init, more info at https://github.com/ellie/atuin#shell-plugin"
}

