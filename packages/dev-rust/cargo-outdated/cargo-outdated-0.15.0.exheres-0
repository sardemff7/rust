# Copyright 2016 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cargo [ rust_minimum_version=1.73.0 ]

SUMMARY="A cargo subcommand for displaying when Rust dependencies are out of date"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv8"

MYOPTIONS="providers: ( libressl openssl ) [[ number-selected = exactly-one ]]"

# The supported LibreSSL and OpenSSL versions are taken from the `version_error()`
# function of openssl-sys 0.9.94, which is what is specified by the Cargo.lock of
# cargo-outdated 0.15.0
DEPENDENCIES="
    build+run:
        providers:libressl? ( dev-libs/libressl:=[>=2.5&<=3.9.1] )
        providers:openssl?  ( dev-libs/openssl:= )
"

# Don't let the openssl-sys crate use a vendored openssl
OPENSSL_NO_VENDOR=1

