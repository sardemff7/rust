# Copyright 2018 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cargo [ crate=fd-find rust_minimum_version=1.77.2 ]
require github [ user=sharkdp tag=v${PV} ]
require bash-completion zsh-completion

SUMMARY="fd is a simple, fast and user-friendly alternative to find"

LICENCES="|| ( Apache-2.0 MIT )"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"

DEPENDENCIES="
"

BASH_COMPLETIONS=( "${WORK}/autocomplete/${PN}.bash" )
ZSH_COMPLETIONS=( "${WORK}/contrib/completion/_${PN}" )

src_compile() {
    SHELL_COMPLETIONS_DIR="${WORK}/completions" \
        cargo_src_compile
}

src_install() {
    cargo_src_install
    doman doc/${PN}.1
    if option bash-completion ; then
        # Only need for bash, fish, etc, not zsh
        emake completions EXE=target/$(rust_target_arch_name)/release/${PN}
        bash-completion_src_install
    fi
    zsh-completion_src_install
}

